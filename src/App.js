import React, { Component } from 'react'
import Table from './Component/Table';
import axios from 'axios';
import faker from 'faker';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit,faTrash,faAngleLeft } from '@fortawesome/free-solid-svg-icons'

export default class App extends Component {
  constructor(){
    super();
    this.state={
      data:[]
    };
  }



  render() {
    const dataTable={
      data:[{
        id:1,
        avatar:<img width="50px" height="50px" src={faker.image.avatar()}/>,
        name:faker.name.firstName(),
        email:faker.internet.exampleEmail(),
        password:faker.internet.password(),
        // operations:[<FontAwesomeIcon onClick={()=>{}} icon={faTrash} />,<FontAwesomeIcon icon={faTrash} />]
      },{
        id:2,
        avatar:<img width="50px" height="50px" src={faker.image.avatar()}/>,
        name:faker.name.firstName(),
        email:faker.internet.exampleEmail(),
        password:faker.internet.password()
      },{
        id:3,
        avatar:<img width="50px" height="50px" src={faker.image.avatar()}/>,
        name:faker.name.firstName(),
        email:faker.internet.exampleEmail(),
        password:faker.internet.password()
      },{
        id:4,
        avatar:<img width="50px" height="50px" src={faker.image.avatar()}/>,
        name:faker.name.firstName(),
        email:faker.internet.exampleEmail(),
        password:faker.internet.password()
      },{
        id:5,
        avatar:<img width="50px" height="50px" src={faker.image.avatar()}/>,
        name:faker.name.firstName(),
        email:faker.internet.exampleEmail(),
        password:faker.internet.password()
      },{
        id:6,
        avatar:<img width="50px" height="50px" src={faker.image.avatar()}/>,
        name:faker.name.firstName(),
        email:faker.internet.exampleEmail(),
        password:faker.internet.password()
      },{
        id:7,
        avatar:<img width="50px" height="50px" src={faker.image.avatar()}/>,
        name:faker.name.firstName(),
        email:'behnambssb313@gmail.com',
        password:faker.internet.password()
      },{
        id:8,
        avatar:<img width="50px" height="50px" src={faker.image.avatar()}/>,
        name:faker.name.firstName(),
        email:'behnambssb313@gmail.com',
        password:faker.internet.password()
      }
        ],
      columns:{
        id:{
          title:'شناسه',
          width:'50px',
          click:item=>{
            return <FontAwesomeIcon icon={faTrash} />
          }
        },
        avatar:{
          title:'تصویر',
          width:'100px'
        },
        name:{
          title:'نام',
          width:'100px'
        },
        email:{
          title:'ایمیل',
          width:'300px'
        },
        password:{
          title:'رمز عبور',
          width:'200px'
        },
        // operations:{
        //   title:'عملیات'
        // }
      }
    }
    return (
      <div className="container" style={{display:'flex',justifyContent:'center',alignItems:'center',height:'900px'}}>
         <Table {...dataTable} da={this.state.data}/>
      </div>
    )
  }
}
