import React from 'react'

export default function Header({columns}) {
    
    const HeaderColumns=Object.keys(columns).map(key=>{
        return <th style={{width:columns[key].width}}><a onClick={(e)=>{return columns[key].click ? columns[key].click(columns[key].title) : null}}>{columns[key].title}</a></th>
    })
    return (
        <thead>
            <tr>
                {HeaderColumns}
            </tr>
        </thead>
        
    )
}
